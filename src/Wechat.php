<?php
declare(strict_types=1);

namespace Xho\Wechat;


class Wechat extends AbstractWechat
{

    /**
     * 公众平台API接口请求
     * @param string $path api接口地址 /api/xxx
     * @param int $type 请求类型 0:get,1:post
     * @param array|string $data 要传递的数组 post默认对数组进行 Json_encode处理，如果要传递xml请直接传字符串
     * @return string
     */
    public function api(string $path, int $type=0, array|string $data=[]): string
    {
        $url=$this->api.$path.'?access_token='.$this->getAccessToken($this->appid);
        if($type){
            if( $data && is_array($data) )
                $data = json_encode($data,JSON_UNESCAPED_UNICODE);
            return Curl::post($url,$data);
        }else{
            if($data && is_array($data))
                $url.='&'. http_build_query( $data );
            return Curl::get($url);
        }
    }

    /**
     * 验证服务器IP或信息有效性
     * @return bool
     * @throws \Exception
     */
    public function checkSignature(): bool
    {
        if (empty($this->token))
            throw new \Exception('TOKEN 没有设置!');
        try {
            $signature = $this->get['signature'];
            $timestamp = $this->get['timestamp'];
            $nonce = $this->get["nonce"];
            $token = $this->token;
            $tmpArr = array($token, $timestamp, $nonce);
            sort($tmpArr, SORT_STRING);
            $tmpStr = implode( $tmpArr );
            $tmpStr = sha1( $tmpStr );
        } catch (\Throwable $th) {
            return false;
        }
        if( $tmpStr == $signature )
            return true;
        else
            return false;
    }

    /**
     * 接收加密信息
     * @param array $data
     * @return mixed
     */
    public function decryptData(array $data): mixed
    {
        $encrypt = $data['Encrypt'];
        $ToUserName = $data['ToUserName'];
        $msg_sign=$this->get['msg_signature'];
        $timeStamp=$this->get['timestamp'];
        $nonce=$this->get['nonce'];
        $format = "<xml><ToUserName><![CDATA[%s]]></ToUserName><Encrypt><![CDATA[%s]]></Encrypt></xml>";
        $from_xml = sprintf($format, $ToUserName , $encrypt);
        $msg = '';
        $errCode = $this->pc->decryptMsg($msg_sign, $timeStamp, $nonce, $from_xml, $msg);
        if ($errCode == 0) {
            try {
                $xml_parser = xml_parser_create();
                if(!xml_parse($xml_parser,$msg,true)){
                    $data =  json_decode($msg);
                }else{
                    $data =  simplexml_load_string($msg, 'SimpleXMLElement', LIBXML_NOCDATA);
                }
                xml_parser_free($xml_parser);
                if($data){
                    return $data;
                }
            } catch (\Exception $e) {
                return false;
            }

        }
        return false;
    }

    /**
     * 获取加密后的数据
     *
     * @param string $xml
     * @return string|null
     */
    public function get_send_string(string $xml): ?string
    {
        if($this->mode > 0){
            $xmlb=null;
            $timeStamp=$this->get['timestamp'];
            $nonce=$this->get['nonce'];
            if($this->pc->encryptMsg($xml,$timeStamp,$nonce,$xmlb) == 0)
                $xml=$xmlb;
        }
        return $xml;
    }

    /**
     * 处理服务器事件路由
     *
     * @param object $data
     * @return string
     */
    public function check_msg_type(object $data): string
    {
        try {
            //路由函数名生成
            $function_name = 'msg_'.$data->MsgType;

            //事件推送路由函数名
            if( $data->MsgType == 'event' )
                $function_name = 'event_'.$data->Event;

            //判断函数是否定义
            if(! method_exists($this,$function_name))
                $function_name = "msg_default";
        } catch (\Throwable $th) {
            $function_name = "msg_default";
        }


        //调用相应处理函数
        return call_user_func(array($this,$function_name),$data);

    }
}